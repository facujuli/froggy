
/*    File:   main.c      */

#include <stdio.h>
#include <stdlib.h>

#include "object.h"
#include "init.h"

#ifndef RASPI
/*****************************************/
    //#include <allegro5/allegro.h>
    //#include <allegro5/allegro_image.h>
    //#include <allegro5/allegro_color.h>
    
    #include "display.h"     //[nacho]
    #include "position.h" 
    #include "game_on.h"
    #include "allegro.h"

/*****************************************/
#endif

OBJECT frog = {NULL, 14, CUADRADITO_SIZE, 3, 0, 0, 1};       //Definición e inicialización de la structura que contiene información sobre la rana
OBJECT car1, car2, car3, car4, truck, tortu1, tortu2, tronco1, tronco2;                                                                 //Declaración de los objetos movibles que apareceran en la pantalla
OBJECT* objetos[OBJ_MAX] = {&frog, &car1, &car2, &car3, &car4, &truck, &tortu1, &tortu2, &tronco1, &tronco2};                           //Arreglo de punteros a todas las estructuras de los objetos vistos en pantalla
int done;

char vidas;

int main(void) 
{
    
    int nivel = 0;
    
    //printf("main 1\n");
    
    if(allegro_init())
    {
        printf("Fallé realizando las inicializaciones de Allegro.\n");
        return -1;
    }
 
    //printf("main 2\n");
    
    vidas = 3;
    
    juguete();     //Funcion donde vive el juego. Se encarga de mostrar cosas en pantalla y destruir, ver comentarios en display.h
    
    //printf("main 3\n");
    
    destroy_allegro();  //funcion para destruir funciones allegro. Antes se encontraba en la funcion juguete
    
    //LIBERACION DE MEMORIA SOLICITADA CON MALLOC en init.c
    
    free_memory();
    
    //printf("main FIN\n");
    
    return 0;
}


