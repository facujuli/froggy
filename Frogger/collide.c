/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/*  Locales     */
#include "object.h"
#include "init.h"


extern OBJECT* objetos[OBJ_MAX];
float on_board_speed;
extern int done;

int collide(void)
{
    int i, j, k, value = 0;
    float n;        //coordenada x del vehiculo
   
    for(i = 1; i < OBJ_MAX; i++)
    {
        if(objetos[i]->y == objetos[FROG]->y)
        {
            for(k = 0; k < objetos[i]->screen_rep; k++)
            {
                for(j = 0; j < objetos[i]->cant_squares; j++ )    //evaluo choque
                {

                    n = round_if(objetos[i]->x[k] + j, objetos[i]->speed);  //evalua la coordenada x del vehiculo, y en base a su velocidad,
                                                                                                //decide si aanzarlo un bloque en el bitmap o no
                    if((int)objetos[FROG]->x[0] == (int)n)
                    {
                        printf("OBJ, X =%f\tN x = %f\t rana =  %d\n", objetos[i]->x[k] + j,n, (int)objetos[FROG]->x[0]);      //para debugear, si queremos que finalice el juego reemplazar po r: done = 1;
                        value ++;      //hubo choque
                    }
                }
            }
        }
        
    }
    return value;
}