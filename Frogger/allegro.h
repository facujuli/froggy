/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   allegro.h
 * Author: facundo
 *
 * Created on December 30, 2022, 4:54 PM
 */

#ifndef ALLEGRO_H
#define ALLEGRO_H

/***********************************************************************************************
 * __ALLEGRO_INIT__
 * 
 * Esta función se encarga de realizar las inicializaciones de ALLEGRO, devolviendo -1 si falla.
 * Dentro de esta, se encuentran impresiones de mensajes con su respectivo error.
 * También se encarga de registrar los eventos en la cola e iniciar el temporizador (timer).
 **********************************************************************************************/
int allegro_init(void);

void destroy_allegro(void);

#endif /* ALLEGRO_H */

