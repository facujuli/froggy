/*  Librerias Estandar  */
#include <stdio.h>

/*  Librerias Allegro   */
#include <allegro5/allegro.h>

#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>

/*  Librerias Locales   */
#include "sound.h"

#define SOUND_MAX 5

static ALLEGRO_SAMPLE* sound_effects[SOUND_MAX];    //  0 -> death_sound        3 -> menu_select_sound    
                                                    //  1 -> frog_movement      4 -> win_sound
                                                    //  2 -> menu_sound

static ALLEGRO_AUDIO_STREAM* music;                 //musica de fondo 


void load_sound_effects(int sound_flag)    // [flag == 0] -> initialize sound effects and initialize music
{                                          // [flag == 1] -> delete_ sound effects and delete music
    int j;
    switch(sound_flag)
    {
        case 0: //caso de inicializar effectos de sonido + musica
            sound_effects[0] = al_load_sample("sounds/effects/death.wav");
            sound_effects[1] = al_load_sample("sounds/effects/frog_movement.wav");
            sound_effects[2] = al_load_sample("sounds/effects/menu.wav");
            sound_effects[3] = al_load_sample("sounds/effects/menu_select.wav");
            sound_effects[4] = al_load_sample("sounds/effects/win.wav");
            
            for(j = 0; j <SOUND_MAX; ++j)
            {
                if(sound_effects[j] == NULL)
                    printf("Error al cargar efecto de sonido sound_effects[%d]\n",j);
            }
            
            music = al_load_audio_stream("sounds/music2.opus", 2, 2048);
            if(music == NULL)
            {
                printf("Error al cargar musica\n");
            }
            else
            {
                al_set_audio_stream_playmode(music, ALLEGRO_PLAYMODE_LOOP);
                al_attach_audio_stream_to_mixer(music, al_get_default_mixer());
            }
            break;
        
        case 1: //caso de destruir efectos de sonido + musica

            for(int i = 0; i < SOUND_MAX; ++i)
            {
                al_destroy_sample(sound_effects[i]);
            }
            al_destroy_audio_stream(music);
            printf("Se destruyo toda la musica correctamente\n");
            break;
    }
    
}

void play_sound(int sound_key)
{
    al_play_sample( sound_effects[sound_key],   // the sample to play
                   1.0,                         // gain ('volume')
                   0.0,                         // pan ('balance')
                   1.0,                         // speed
                   ALLEGRO_PLAYMODE_ONCE,       // play mode
                   NULL                         // sample id
                   );
}