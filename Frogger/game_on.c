/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <pthread.h>

#include "display.h"           
#include "keyboard_thread.h"   
#include  "position.h"
#include "game_on.h"

void juguete(void) 
{ 
    pthread_t t1,t2;
    
    pthread_create(&t2, NULL, &keyboard_thread, NULL);
    
    pthread_create(&t1, NULL, &display_thread, NULL);
    
    pthread_join(t2,NULL);  // Arranca el teclado por su cuenta (hasta que se cierre el juego o se presione escape)
    
    printf("fin de keyboard thread\n");
    
    pthread_join(t1,NULL);  // Arranca el display por su cuenta (hasta que se cierre el juego o se presione escape)
    
    printf("fin de display thread \n");
    
    return;
}