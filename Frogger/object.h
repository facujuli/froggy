
#ifndef OBJECT_H
#define OBJECT_H

/**************************************************************
*    En la siguiente estructura se guardará                   *
*    la información de cada objeto (rana, arbol, auto, etc)   *
* 
*   NOTA: Cada objeto posee una sola estructura, con sus determinados datos. 
*   A la hora de mostrara  este objeto en pantalla, se copia al objeto "separation" pixeles
*   a su derecha, "screen_rep" veces.
* 
*   _EJ:_ si para el tronco screen_rep = 4, y separation = 200,  el tronco se mostrara máximo 4 veces
*   en pantalla, con una separación de 200 pixeles (valor simbólico).
**************************************************************/


typedef struct
{
    float* x;                //puntero a FLOAT (puntero a  primer elemento de un arreglo de floats)
    int y;                  //coordenada y
    int height;             //altura en pixeles
    float speed;            //velocidad (en cada vuelta, el objeto se movera "speed" veces a su derecha, acepta negaativos).
    int separation;         //separación entre objetos del mismo tipo en pantalla
    int screen_rep;         //cantidad maxima de veces que se muestra en pantalla al mismo tiempo
    int cant_squares;       //variable que guarda el valor de la cantidad de cuadrados que ocupa en cada vehiculo
}OBJECT;

/* SE PUEDE HACER ESTO????????????????????*/


#define OBJ_MAX 10      //cant maxima de objets
#define MENUS_MAX 5     //cantidad de menus

enum MYOBJECTS_PLUS_BACKGROUND_PLUS_MENUS      //macros asociadas a los objetos
{
    FROG, CAR1, CAR2, CAR3, CAR4, TRUCK, TORTU1, TORTU2, TRONCO1, TRONCO2, BACKGROUND, MAIN_MENU_1, MAIN_MENU_2, MAIN_MENU_3, MAIN_MENU_4, PAUSE_MENU
};
//  0       1     2     3    4      5       6       7       8       9          10         11            12           13           14          15        


#endif /* OBJECT_H */

