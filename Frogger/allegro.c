
/*  Librerias clasicas  */
#include <stdio.h>

/*  Librerias de allegro  */
#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_color.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>

/*  Librerias locales  */
#include "allegro.h"
#include "display.h"

ALLEGRO_DISPLAY *display;
ALLEGRO_TIMER *timer;
ALLEGRO_EVENT_QUEUE* event_queue;
ALLEGRO_BITMAP *menu;

ALLEGRO_KEYBOARD_STATE ks;  //[nacho]
ALLEGRO_EVENT event;        //[nacho]


int allegro_init(void)
{
    if(!al_init())
    {
        printf("No pude iniciar Allegro.\n");
        return -1;
    }
    
    if (!al_init_primitives_addon()) 
    {
        fprintf(stderr, "failed to initialize primitives!\n");
        return -1;
    }
    
    if(!al_install_mouse())
    {
        al_shutdown_primitives_addon();
        printf("No pude instalar el Mouse\n");
        return -1;
    }
    
    event_queue = al_create_event_queue();
    if (!event_queue)
    {
        al_shutdown_primitives_addon();
        printf("No pude crear la cola.\n");
        return -1;
    }
    
    if (!al_install_keyboard()) 
    {
        fprintf(stderr, "failed to initialize the keyboard!\n");
        al_shutdown_primitives_addon();
        al_destroy_event_queue(event_queue);
        return -1;
    }
    
    timer = al_create_timer(1.0 /FPS);
    if(!timer)
    {
        al_shutdown_primitives_addon();
        al_destroy_event_queue(event_queue);
        printf("No pude crear el timer.\n");
        return -1;
    }
    
    if(!al_init_image_addon())
    {
        printf("No pude hacer el addon.\n");
        al_shutdown_primitives_addon();
        al_destroy_event_queue(event_queue);
        al_destroy_timer(timer);
        return -1;
    }
    
    if(!al_install_audio() || !al_init_acodec_addon() || !al_reserve_samples(6) )
    {
        printf("No pude instalar la musica.\n");
        al_shutdown_image_addon();
        al_shutdown_primitives_addon();
        al_destroy_event_queue(event_queue);
        al_destroy_timer(timer);
        return -1;
    }
    
    

    display = al_create_display(SIZE_W, SIZE_H);    //create display siempre ultimo en crearse(por si hay errores)
    if(!display)
    {
        printf("No pude crear el display.\n");
        al_shutdown_image_addon();
        al_shutdown_primitives_addon();
        al_destroy_event_queue(event_queue);
        al_destroy_timer(timer);
        return -1;
    }
    
    
    
    al_register_event_source(event_queue, al_get_display_event_source(display));
    al_register_event_source(event_queue, al_get_timer_event_source(timer));
    al_register_event_source(event_queue, al_get_mouse_event_source());
    al_register_event_source(event_queue, al_get_keyboard_event_source());
    
    
    al_clear_to_color(al_map_rgb(0,255,255));
    al_start_timer(timer);
    
    
    return 0;
}

void destroy_allegro(void)
{
    al_shutdown_primitives_addon();
    al_shutdown_image_addon();
    al_destroy_timer(timer);
    al_destroy_display(display);
    al_destroy_event_queue(event_queue);
    printf("Destrui todo lo de allegro correctamente\n");
}
