/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*  Estandares  */
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/*  Locales     */
#include "object.h"
#include "init.h"
#include "collide.h"
#ifndef RASPI
/****************************************/
    #include <allegro5/allegro.h>
    #include <allegro5/allegro_image.h>
    #include <allegro5/allegro_color.h>

    #include "display.h" //[nacho]
/****************************************/
#endif

#define VEL(v) (v*(3.0/FPS)/20.0)

#define MIN_SEP_CAR 2   //separacion minima que deben tener los coches entre si
#define MIN_SEP_TRUCK 3   //separacion minima que deben tener los camiones entre si
#define MIN_SEP_BOAT 5   //separacion minima que deben tener los botes entre si

extern OBJECT* objetos[OBJ_MAX];
float on_board_speed;
extern int done;
void evaluate_sep(int i);


void initialize_objects(int nivel)       //Los objetos deberán ser inicializados en cada nivel. 
{
    srand(time(NULL));
    
    float speed[OBJ_MAX] = {VEL(1), VEL(1), VEL(1), -VEL(1), -VEL(1), VEL(1), -VEL(1), VEL(1), VEL(1), VEL(1)};     //Arreglo que contendrá en cada columna las respectivas velocidades de los objetos.
                                                                                                                       //cada fila representará un nivel distinto
    int separation[OBJ_MAX] = { 0, 4, 4, 4, 4, 5, 6, 6, 6, 7 }; //Arreglo que contendrá en cada columna las respectivas separaciones 
                                                                                                                                         //q posseen entre si los objetos
    int screen_rep[OBJ_MAX] = { 0, 3, 3, 3, 4, 3, 3, 3, 3, 3 };         //Arreglo asociada que guarda cuantas veces aparece el objeto por pantalla, en el nivel
                                                                                                             //si screen_rep[m][j] = 0, significa que en el nivel m, el objeto j no aparece.
    int coor_y[OBJ_MAX] = {0, 13, 12, 11, 10, 9, 7, 6, 5, 4};
    
    int squares_cant[OBJ_MAX] = {1,1,1,1,1,2,3,3,3,3};
    
    
    int i;
    for(i = 1; i < OBJ_MAX; i++)
    {
    /********************************************
     * 
     * Lo siguiente es la inicialización de cada estructura de los objetos,
     * reemplazando cada valor de la mencionada, por su respectivo valor en
     * el arreglo designado.
     * 
     ********************************************/
        
        objetos[i]->separation = separation[i];
        objetos[i]->y = coor_y[i];
        objetos[i]->speed = speed[i];
        objetos[i]->cant_squares = squares_cant[i];
        objetos[i]->screen_rep = screen_rep[i];
        objetos[i]->x [0] =  rand() % 17;            //se designa la coordenada x de la primer variacion del objeto de manera aleatoria con un numero <= 16;

        objetos[i]->x = (float*) malloc(objetos[i]->screen_rep * sizeof(float)); // creo una especie de arreglo que contendra la coordenada en X de cada copia de este objeto.
        
        
        int k;
        //printf("SINVAR: \t X:%f\t  objeto %d  variacion%d\n", objetos[i]->x[0], i,0);
        for(k = 1; k < objetos[i]->screen_rep; k++)
        {
            objetos[i]->x [k] = (int) (objetos[i]->x [k-1] + objetos[i]->separation) % 17; //inicializo los valores de cada coordenada x del objeto, separadas homogeneamente
            
        }
        evaluate_sep(i);

        
        #ifndef RASPI
        /********************************************************************************************/
            if(load_image(nivel, i))
            {
                printf("Fallé inicializando el bitmap del objeto[ %d ] en el nivel %d.\n", i, nivel);
            }
        /********************************************************************************************/
        #endif
    }
    objetos[FROG]->x = (float*) malloc(sizeof(float));
    objetos[FROG]->x [0] = 8;
    
    #ifndef RASPI
    /***************************************/
        load_image(nivel, FROG);
        load_image(nivel, BACKGROUND);
        
        int i_menus;
        for(i_menus = MAIN_MENU_1; i_menus <= PAUSE_MENU; ++i_menus )
            load_image(nivel, i_menus);
    /***************************************/
    #endif
    
    return;
}

void move_objects (void)
{
    if( (objetos[FROG]->y <= objetos[TORTU1]->y) && (objetos[FROG]->y >= objetos[TRONCO2]->y)   )   //Si la rana se halla en la zona de agua
    {
        ;
    }
    else
    {
        objetos[FROG]->x[0] = (int) objetos[FROG]->x[0];  //La rana no se halla en un bote su coordenada x debe ser un numero entero entre 0 y 15. 
    }
    
    int i,k;
    for ( i = 1; i < OBJ_MAX; i++)
    {
        for(k = 0; k < objetos[i]->screen_rep; k++)
        {
            objetos[i]->x[k] += objetos[i]->speed;     //aumento la posicion en x speed veces

            if(objetos[i]->speed > 0 && objetos[i]->x [k]> 16)
            {
                objetos[i]-> x [k] = (-1)*(objetos[i]->cant_squares);  //si salio de la pantalla por el lado derecho, lo corro su ancho a la izquierda del mapa
            }
            else if( objetos[i]->speed < 0 && objetos[i]-> x [k] < (-1)*(objetos[i]->cant_squares))
            {
                objetos[i]->x[k] = 16 + objetos[i]->cant_squares; // si salio de la pantalla por el lado izq, lo corro su ancho para el lado derecho
            }
        }
    }
/*
    if(collide() && objetos[FROG]->y >= TRUCK)
    {
        static int s;
        printf("choque_ %d\n", s++);
    }
    else if( (!collide()) && objetos[FROG]->y <= TRONCO2)
    {
        static int r;
        printf("Rana ahogada: %d\n", r++);
    }
*/
}


float round_if(float n, float speed)
{
    if(speed > 0)       //si la velocidad es mayor a 0
    {
        if(n >= (int)n + 0.5)       //redondeo al entero proximo mayor
        {
            return ( (int)n+1 );
        }
        else
        {
            return n;
        }
    }
    else if(speed < 0)
    {
        
        if(n <= (int)n + 0.5)       
        {
            return ( (int)n );  
        }
        else
        {
            return n + 1;
        }
    }
}

void free_memory(void)
{
    int i;
    for ( i = 0; i < OBJ_MAX; +i++)
    {
        free(objetos[i]->x);  //libero la memoria solicitada con mmalloc en la funcion initialize_objects
        printf("Libere\tcorrectamente\tlas\tx\tde :\t%d\n", i);
    }
}

void evaluate_sep(int macro_type)   //evalua que los objetos no se encuentren pegados, en ese caso, les modifica las posiciones para evitarlo.
{
    int k, min_sep, modulo, flag = 0;
    
    if( macro_type < TRUCK)
    {
        min_sep = MIN_SEP_CAR;      //esta variable contiene la separacion minima que establecimos deben tener los vehiculos
    }
    else if(macro_type == TRUCK)
    {
        min_sep = MIN_SEP_TRUCK;    //esta variable contiene la separacion minima que establecimos deben tener los vehiculos
    }
    else
    {
        min_sep = MIN_SEP_BOAT; //esta variable contiene la separacion minima que establecimos deben tener los vehiculos
    }
    
    for(k = 1; k < objetos[macro_type]->screen_rep; k++)        //recorre todas las variaciones del vehiculo
    {
        modulo = objetos[macro_type]->x[k] - objetos[macro_type]->x[k - 1];     //evalua separacion de vehiculos entre si
        if(modulo < 0)
        {
            modulo *= -1;
        }
        
        if(modulo < min_sep)
        {
            flag = 1;
        }
    }
    
    modulo = objetos[macro_type]->x[k-1] - objetos[macro_type]->x[0];     //comparo el primero con el ultimo
    
    if(modulo < 0)
    {
        modulo *= -1;
    }
    //printf("\t\t\tMODULO:    %d\n", modulo);
    if(modulo < min_sep)
    {
        flag = 1;
    }
 
    if(flag)
    {
        objetos[macro_type]->x[0] = 0;
        //printf("CONVAR: \t X:%f\t  objeto %d  variacion%d\n", objetos[macro_type]->x[0], macro_type,0);
        for(k = 1; k < objetos[macro_type]->screen_rep; k++)
        {
            objetos[macro_type]->x [k] = (int) (objetos[macro_type]->x [k-1] + objetos[macro_type]->separation) % 17; //inicializo los valores de cada coordenada x del objeto, separadas homogeneamente
            //printf("CONVAR: \t X:%f\t  objeto %d  variacion%d\n", objetos[macro_type]->x[k], macro_type,k);
        }
    }
}