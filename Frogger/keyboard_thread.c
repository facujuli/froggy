/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
#include <stdio.h>

/*  allegro */
#include <allegro5/allegro.h>

#include "display.h"
#include  "keyboard_thread.h"
#include "allegro.h"
#include "position.h" 


enum keys {KEY_NO_OPERATION ,KEY_UP, KEY_DOWN, KEY_LEFT, KEY_RIGHT,KEY_SELECTION, KEY_ESCAPE, KEY_PAUSE, KEY_MUTE};  

int keyboard_redraw;      // dato compartido con display. Permite refrescar el display acorde a FPS
int key_pressed;          // dato que lee que tecla se presiono
int state_pause;          // dato que togglea el pausa del juego
int state_mute;           // dato que togglea el volumen

extern int done;                 // dato compartido con display. Determina si se termina todo el programa

extern ALLEGRO_DISPLAY *display;
extern ALLEGRO_TIMER *timer;
extern ALLEGRO_EVENT_QUEUE* event_queue;

extern ALLEGRO_KEYBOARD_STATE ks;  
extern ALLEGRO_EVENT event;        


void * keyboard_thread(void* none)
{  
    while( done == 0 )   // mientras el programa se encuentre encendido
    {
        al_wait_for_event(event_queue, &event);   //espera el evento

        switch(event.type)  // Se analiza que tipo de evento es
        {
            case ALLEGRO_EVENT_TIMER:   //Si es un evento del timer (determinado por FPS en display.h) se actualiza el display  
                keyboard_redraw = 1;
                break;
                
            case ALLEGRO_EVENT_KEY_DOWN:    //Si se preiono una tecla, evaluo cual
                if(event.keyboard.keycode == ALLEGRO_KEY_UP)
                {
                    check_display_status(KEY_UP);  //Evaluo en que pantalla se encuentra el display(para saber para que estoy presionando la tecla) 
                }
                if(event.keyboard.keycode == ALLEGRO_KEY_DOWN)
                {
                    check_display_status(KEY_DOWN); //Evaluo en que pantalla se encuentra el display(para saber para que estoy presionando la tecla)
                }
                if(event.keyboard.keycode == ALLEGRO_KEY_LEFT)
                {
                    check_display_status(KEY_LEFT); //Evaluo en que pantalla se encuentra el display(para saber para que estoy presionando la tecla)
                }
                if(event.keyboard.keycode == ALLEGRO_KEY_RIGHT)
                {
                    check_display_status(KEY_RIGHT); //Evaluo en que pantalla se encuentra el display(para saber para que estoy presionando la tecla)
                }
                if(event.keyboard.keycode == ALLEGRO_KEY_ENTER || event.keyboard.keycode == ALLEGRO_KEY_SPACE || event.keyboard.keycode == ALLEGRO_KEY_X)
                {
                    check_display_status(KEY_SELECTION); //Evaluo en que pantalla se encuentra el display(para saber para que estoy presionando la tecla)
                }
                if(event.keyboard.keycode == ALLEGRO_KEY_ESCAPE)    //boton de escape(salir del programa)
                {
                    key_pressed = KEY_ESCAPE;
                    done = 1;
                }
                if(event.keyboard.keycode != ALLEGRO_KEY_ESCAPE)
                    break;
                    
            case ALLEGRO_EVENT_DISPLAY_CLOSE:
                done = 1;
                key_pressed = KEY_ESCAPE;   
                break;
        }
    }
    return NULL; //salgo del thread
}