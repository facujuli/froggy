/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
#include <stdio.h>
#include <pthread.h>
#include <time.h>

/*  allegro */
#include <allegro5/allegro.h>

#include "display.h"
#include  "keyboard_thread.h"
#include  "position.h"

#include "allegro.h"
#include "object.h"
#include "sound.h"



extern int menu_status;
extern int key_pressed;
extern OBJECT* objetos[];
extern int menu_status;
extern float rotation_flag;
extern selection select_option;

enum keys {KEY_NO_OPERATION ,KEY_UP, KEY_DOWN, KEY_LEFT, KEY_RIGHT,KEY_SELECTION, KEY_ESCAPE, KEY_PAUSE, KEY_MUTE};



void check_display_status(int key)  //Evalua al presionar una tecla en que pantalla me encuentro(juego, pausa, principal, etc)
{
    if(menu_status == 0)    // Me encuentro en Juego.
    {
        play_sound(1);      // Suena sonido de movimiento. Lista de sonidos se encuentra en sound.c
        position_frog(key); // Evaluo si se puede mover la rana(por ejemplo, si esta en un borde a la izquierda y se presiona la tecla <- )
    }
    
    else if(menu_status > 0)
    {
        play_sound(2);      // Suena sonido de seleccion. Lista de sonidos se encuentra en sound.c
        if(key == KEY_UP)   // En vez de usar if(key == ...) usaria switchs.
        {
            check_position(KEY_UP); //Evaluo a donde puedo mover la seleccion del menu
        }
        else if(key == KEY_DOWN)
        {
            check_position(KEY_DOWN);   //Evaluo a donde puedo mover la seleccion del menu
        }
        else if(key == KEY_LEFT)
        {
            check_position(KEY_LEFT);   //Evaluo a donde puedo mover la seleccion del menu
        }
        else if(key == KEY_RIGHT)
        {
            check_position(KEY_RIGHT);  //Evaluo a donde puedo mover la seleccion del menu
        }
        else if(key == KEY_SELECTION)
        {
            select_option.pressed = 1;  //se presiono ENTER o BARRA o x que son las de seleccion
        }
    }  
    
    else    //de alguna manera se rompio el menu_status
        printf("Error de pantalla de menu\n");
}



void position_frog(int key) //Funcion evalua si se puede mover la rana
{       
    if( (key == KEY_UP) )   
    {
        if( check_position(KEY_UP) )    //evalua si se puede mover la rana. Si puede, entra al if, de lo contrario esta chocando con una pared
        {
            key_pressed = KEY_UP;
            (objetos[FROG] -> y) --;
            rotation_flag = 0;
            printf("x:%d  y:%d \n",(int)(objetos[FROG] -> x[0]),(objetos[FROG] -> y));
        }
        else
            printf("Wall\n");
    }
    
    
    else if( (key == KEY_DOWN) ) 
    {
        if( check_position(KEY_DOWN) )  //evalua si se puede mover la rana. Si puede, entra al if, de lo contrario esta chocando con una pared
        {
            key_pressed = KEY_DOWN;     // este dato es compartido con display.c para que sepa a donde esta orientado el sapo
            (objetos[FROG] -> y) ++;    //se modifica la coordenada por un CUADRADITO_SIZE 
            rotation_flag = ALLEGRO_FLIP_VERTICAL;
            printf("x:%d  y:%d \n",(int)(objetos[FROG] -> x[0]),(objetos[FROG] -> y));
        }
        else
            printf("Wall\n");
    }
    
    
    else if( (key == KEY_LEFT) )
    {   
        if( check_position(KEY_LEFT) )  //evalua si se puede mover la rana. Si puede, entra al if, de lo contrario esta chocando con una pared
        {
            key_pressed = KEY_LEFT; // este dato es compartido con display.c para que sepa a donde esta orientado el sapo
            (objetos[FROG] -> x[0]) --;    //se modifica la coordenada por un CUADRADITO_SIZE 
            rotation_flag = 0;
            printf("x:%d  y:%d \n",(int)(objetos[FROG] -> x[0]),(objetos[FROG] -> y));
        }
        else
            printf("Wall\n");
    }
    
    
    else if( (key == KEY_RIGHT) )
    {
        if( check_position(KEY_RIGHT) ) //evalua si se puede mover la rana. Si puede, entra al if, de lo contrario esta chocando con una pared
        {
            key_pressed = KEY_RIGHT;    // este dato es compartido con display.c para que sepa a donde esta orientado el sapo
            (objetos[FROG] -> x[0]) ++;    //se modifica la coordenada por un CUADRADITO_SIZE 
            rotation_flag = 0;
            printf("x:%d  y:%d \n",(int)(objetos[FROG] -> x[0]),(objetos[FROG] -> y));
        }
        else
            printf("Wall\n");
    }
         
}

static int check_position(int keys) //Evalua si es posible el moviemiento. Tanto de la rana como del rectangulo de seleccion.
{
    int state = 0;  // dato si es posible el moviemiento. 0 no es posible, 1 es posible
    
    if(menu_status == 0)    // Me encuentro en pantalla de juego
    {
        switch (keys)   // Analizo que tecla se presiono
        {
            case(KEY_UP):
            {
                if(objetos[FROG]->y > 3)    //Evalua si me encuentro en el borde superior
                {
                    state = 1;
                }
                break;
            }

            case(KEY_DOWN):
            {
                if(objetos[FROG]->y < 14)   //Evalua si me encuentro en el borde inferior
                {
                    state = 1;
                }
                break;
            }

            case(KEY_LEFT):
            {
                if( (objetos[FROG] -> x[0]) > 0)   //Evalua si me encuentro en el borde izquierdo
                {
                    state = 1;
                }
                break;
            }

            case(KEY_RIGHT):
            {
                if( (objetos[FROG] -> x[0]) < 15)    //Evalua si me encuentro en el borde derecho
                {
                    state = 1;
                }
                break;
            }
        } 
    }
    
    else if(menu_status == 1)    //pantalla de menu de PLAY
    {
        switch (keys)   //Analizo que tecla se presiono
        {
            case(KEY_UP):
            {
                if( select_option.y_start == (7*CUADRADITO_SIZE) )  //opcion de "PLAY" a "EXIT"
                {
                    select_option.x_start = 5*CUADRADITO_SIZE; 
                    select_option.y_start = 14*CUADRADITO_SIZE;
                    select_option.x_end = 7*CUADRADITO_SIZE;
                    select_option.y_end = 15*CUADRADITO_SIZE;
                }
                else if(select_option.y_start == (11*CUADRADITO_SIZE) )  //opcion de "LEADERBOARDS" a "PLAY"
                {
                    select_option.x_start = 4*CUADRADITO_SIZE; 
                    select_option.y_start = 7*CUADRADITO_SIZE;  //cambiar a 7
                    select_option.x_end = 8*CUADRADITO_SIZE;    //cambiar a 8 
                    select_option.y_end = 9*CUADRADITO_SIZE; 
                }
                else if(select_option.y_start == (14*CUADRADITO_SIZE))  //opcion de "EXIT" a "LEADERBOARDS"
                {
                    select_option.x_start = 3*CUADRADITO_SIZE; 
                    select_option.y_start = 11*CUADRADITO_SIZE;
                    select_option.x_end = 9*CUADRADITO_SIZE;
                    select_option.y_end = 12*CUADRADITO_SIZE; 
                }
                break;
            }

            case(KEY_DOWN):
            {
                if( select_option.y_start == (7*CUADRADITO_SIZE) )  //opcion de "PLAY" a "SETTINGS"
                {
                    select_option.x_start = 3*CUADRADITO_SIZE; 
                    select_option.y_start = 11*CUADRADITO_SIZE;
                    select_option.x_end = 9*CUADRADITO_SIZE;
                    select_option.y_end = 12*CUADRADITO_SIZE;
                }
                else if(select_option.y_start == (11*CUADRADITO_SIZE) )  //opcion de "SETTINGS" a "EXIT"
                {
                    select_option.x_start = 5*CUADRADITO_SIZE; 
                    select_option.y_start = 14*CUADRADITO_SIZE;
                    select_option.x_end = 7*CUADRADITO_SIZE;
                    select_option.y_end = 15*CUADRADITO_SIZE; 
                }
                else if(select_option.y_start == (14*CUADRADITO_SIZE))  //opcion de "EXIT" a "PLAY"
                {
                    select_option.x_start = 4*CUADRADITO_SIZE; 
                    select_option.y_start = 7*CUADRADITO_SIZE;
                    select_option.x_end = 8*CUADRADITO_SIZE;
                    select_option.y_end = 9*CUADRADITO_SIZE; 
                }
                break;
            }

            case(KEY_LEFT):
            {
                printf("solo flechas arriba y abajo\n");
                break;
            }

            case(KEY_RIGHT):
            {
                printf("solo flechas arriba y abajo\n");
                break;
            }
        }   // [nacho]mantener minimizado
    }
    
    else if (menu_status == 2)  // Pantalla de NICKNAME
    {
        switch (keys) 
        {
            case(KEY_UP):
            {
                if( select_option.y_start == (6*CUADRADITO_SIZE) )  //opcion de "NICKNAME" a "RETURN"
                {
                    select_option.x_start = 3*CUADRADITO_SIZE; 
                    select_option.y_start = 14*CUADRADITO_SIZE;
                    select_option.x_end = 7*CUADRADITO_SIZE;
                    select_option.y_end = 15*CUADRADITO_SIZE;
                }
                else if(select_option.y_start == (14*CUADRADITO_SIZE) )  //opcion de "RETURN" a "NICKNAME"
                {
                    select_option.x_start = 3*CUADRADITO_SIZE; 
                    select_option.y_start = 9*CUADRADITO_SIZE;
                    select_option.x_end = 9*CUADRADITO_SIZE;
                    select_option.y_end = 10*CUADRADITO_SIZE; 
                }
                break;
            }

            case(KEY_DOWN):
            {
                if( select_option.y_start == (9*CUADRADITO_SIZE) )  //opcion de "CHOOSE LEVEL" a "RETURN"
                {
                    select_option.x_start = 3*CUADRADITO_SIZE; 
                    select_option.y_start = 14*CUADRADITO_SIZE;
                    select_option.x_end = 7*CUADRADITO_SIZE;
                    select_option.y_end = 15*CUADRADITO_SIZE;
                }
                else if(select_option.y_start == (14*CUADRADITO_SIZE) )  //opcion de "RETURN" a "CHOOSE LEVEL"
                {
                    select_option.x_start = 3*CUADRADITO_SIZE; 
                    select_option.y_start = 9*CUADRADITO_SIZE;
                    select_option.x_end = 9*CUADRADITO_SIZE;
                    select_option.y_end = 10*CUADRADITO_SIZE;
                }
                break;
            }

            case(KEY_LEFT):
            {
                printf("solo flechas arriba y abajo\n");
                break;
            }

            case(KEY_RIGHT):
            {
                printf("solo flechas arriba y abajo\n");
                break;
            }
        }
    }
    
    else if(menu_status == 3)   // Pantalla de LEADERBOARDS
    {
    //  no operation 
    }
    
    else if (menu_status == 4)  //pantalla de CHOOSE FROG
    {
    switch (keys) 
        {
            case(KEY_UP):
            {
                if(select_option.y_start > 8*CUADRADITO_SIZE)
                { 
                    if(select_option.y_start == 14*CUADRADITO_SIZE)
                        select_option.x_end -= CUADRADITO_SIZE;
                        
                    select_option.y_start -= (2*CUADRADITO_SIZE);
                    select_option.y_end -= (2*CUADRADITO_SIZE);   
                }
                
                break;
            }

            case(KEY_DOWN):
            {
                if(select_option.y_start < 14*CUADRADITO_SIZE)
                {
                    if( (select_option.y_start == 12*CUADRADITO_SIZE) && (select_option.x_start == (7*CUADRADITO_SIZE - CUADRADITO_SIZE/2)) )
                    {}  // no operation (por alguno motivo no funciona con !=)
                    else
                    {
                        select_option.y_start += (2*CUADRADITO_SIZE);
                        select_option.y_end += (2*CUADRADITO_SIZE);
                    }
                    
                    if(select_option.y_start == 14*CUADRADITO_SIZE) //RETURN    
                        select_option.x_end += CUADRADITO_SIZE;
                }
                
                break;
            }

            case(KEY_LEFT):
            {
                if( select_option.x_start > 3*CUADRADITO_SIZE && select_option.y_start != 14*CUADRADITO_SIZE)
                { 
                    select_option.x_start -= (4*CUADRADITO_SIZE - CUADRADITO_SIZE/2);
                    select_option.x_end -= (4*CUADRADITO_SIZE - CUADRADITO_SIZE/2);   
                }
                
                break;
            }

            case(KEY_RIGHT):
            {
                if( select_option.x_start < (7*CUADRADITO_SIZE - CUADRADITO_SIZE/2) && select_option.y_start != 14*CUADRADITO_SIZE )
                { 
                    select_option.x_start += (4*CUADRADITO_SIZE - CUADRADITO_SIZE/2);
                    select_option.x_end += (4*CUADRADITO_SIZE  -CUADRADITO_SIZE/2);   
                }
                
                break;
            }
        }   
    }
    
    else
        printf("error de menu_status\n");
    
    return state;
                               
}




