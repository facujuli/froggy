/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   posicion_frog_thread.h
 * Author: sanmar
 *
 * Created on January 10, 2023, 8:26 PM
 */

#ifndef POSITION_H
#define POSITION_H

//float on_board_speed;

void check_display_status(int keys);

static int check_position(int keys);

void position_frog(int);




/******************************************************************************************
 * __MOVE_OBJECTS__
 * 
 * Función encargada de mover los objetos por el display (hacia la izq. o der.).
 * No mueve a la rana.
 * Solo altera sus coordenadas, no lo imprime en el display, es un thread que funciona tanto
 * para la RPI como para ALLEGRO.
 * 
 * NOTA: esta función es llamada iteradamente en "juguete()", una vez hecho el thread, esta función deberá ser aislada de esos archivos.
 * para así poder ser utilizada tanto por ALLEGRO y RPI.
 ******************************************************************************************/

#endif /* POSITIONS_H */

