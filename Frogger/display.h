/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   display_thread.h
 * Author: sanmar
 *
 * Created on January 10, 2023, 5:27 PM
 */

#ifndef DISPLAY_THREAD_H
#define DISPLAY_THREAD_H



#define FPS 60

#define SIZE_DISPLAY 800 // Size del display multiplo de 16 (ej: 800, 960). Usar en 960 por ahora pls
    #define SIZE_W SIZE_DISPLAY
    #define SIZE_H SIZE_DISPLAY

#define CUADRADITO_SIZE (SIZE_DISPLAY / 16)

#define OBJ_H 100

typedef struct          //estructura para seleccionar opciones [cuadradito amarillo]
{
    int x_start;        //donde comienza coordenada x
    int y_start;        //donde termina coordenada x
    int x_end;          //donde empieza coordenada y
    int y_end;          //donde termina coordenada y
    int pressed;        //si se apreto o no
} selection;



void *display_thread(void*);

int load_image(int nivel, int macro_type);

static void destruir_bitmaps(void);

static int draw_level(int);

static int go_to_main_menu(void);


#endif /* DISPLAY_THREAD_H */

