/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   sound.h
 * Author: sanmar
 *
 * Created on January 23, 2023, 6:08 PM
 */

#ifndef SOUND_H
#define SOUND_H

#define SOUND_MAX 5

void play_sound(int);

void load_sound_effects(int sound_flag);

#endif /* SOUND_H */

