#ifndef INIT_H
#define INIT_H

//extern OBJECT* objetos[OBJ_MAX];

void initialize_objects(int nivel);

void move_objects (void);
float round_if(float, float);
void free_memory(void);

/******************************************************************************************
 * __INITIALIZE_OBJECTS__
 * 
 * Función encargada de inicializar los objetos segun el nivel en que se halle el jugador.
 * Primer parámetro: es un número que representa el tipo de objeto que es. Previamente
 * predefinido por una macro con su respectivo nombre.
 * Segundo parámetro: es el nivel en que se hallá jugando.
 ******************************************************************************************/

#endif