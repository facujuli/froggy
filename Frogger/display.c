/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*  standards */
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <time.h>

/*  allegro */
#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h> 
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_color.h>

/*  locales */
#include "init.h"
#include "display.h"            //[nacho]
#include "keyboard_thread.h"    //[nacho]
#include "position.h"
#include "object.h"
#include "sound.h"

extern ALLEGRO_DISPLAY *display;
extern ALLEGRO_TIMER *timer;
extern ALLEGRO_EVENT_QUEUE* event_queue;
extern ALLEGRO_BITMAP *menu;
extern OBJECT* objetos[];

/*  externs */
extern int key_pressed;         //  dato compartido con keyboard_thread.c                                                       
extern int keyboard_redraw;     //  dato compartido con keyboard_thread.c                                                         
extern int done;
extern char vidas;
static float dibujar_objeto(int macro_type, int k);

float rotation_flag;

enum keys {KEY_NO_OPERATION ,KEY_UP, KEY_DOWN, KEY_LEFT, KEY_RIGHT,KEY_SELECTION, KEY_ESCAPE, KEY_PAUSE, KEY_MUTE};

/*  datos globales de display   */
int go_back;
int menu_status = 1;            //         dato que permite saber en donde me encuentro parado en los menus.
                                //         1 -> menu principal          2 -> nickanme       
                                //         3 -> hall of fame            4 -> frog
                                //         5 -> pause menu              0-> no estoy en el menu

selection select_option = {4*CUADRADITO_SIZE, 7*CUADRADITO_SIZE, 8*CUADRADITO_SIZE, 9*CUADRADITO_SIZE, 0 }; //dato global de seleccion(rectangulo amarillo).
ALLEGRO_BITMAP *bitmaps [OBJ_MAX + MENUS_MAX + 1];      //guarda bitmaps de los objetos + el del display


void *display_thread(void* none)
{   
    while(done == 0)                    // Mientras el programa se encuentre encendido
    {
        if (keyboard_redraw)            // Refresh rate determinado por FPS (display.h). Alterna constantemente entre 0 y 1.
        {
            keyboard_redraw = 0;
            
            if ( go_to_main_menu() == 1 )   // "go_to_main_menu" es una funcion que encierra todas las pantallas posibles. Si se sale de esta, se cierra el programa.
            {
                done = 1;
                printf("Fin \n");
            }
           
        }
    }
    load_sound_effects(1);  //destruyo musica
    destruir_bitmaps();     // si se sale del programa. Se destruye el arreglo de bitmaps  
    
    return NULL;
}

static float dibujar_objeto(int macro_type, int k) //devuelve la coordenada X en donde dibujo al objeto
{ 
   float coor_x = objetos[macro_type]->x[k] * CUADRADITO_SIZE;
   int coor_y = objetos[macro_type]->y*CUADRADITO_SIZE;
   
  // printf("Cordenada X= %f\t del objeto: %d\t Variacion: \t%d\n", objetos[macro_type]->x[k], macro_type, k);
   
   if( macro_type == 0 )    // Si se trata del sapo
    {
        float cx = al_get_bitmap_width(bitmaps[FROG]);
        float cy = al_get_bitmap_height(bitmaps[FROG]);
        float sw = al_get_bitmap_width(bitmaps[macro_type]);
        float sh = al_get_bitmap_height(bitmaps[macro_type]);
        int dw = CUADRADITO_SIZE; 
        int dh = CUADRADITO_SIZE;

        al_draw_scaled_bitmap( bitmaps[macro_type], 0, 0, sw, sh, coor_x, coor_y, dw, dh, rotation_flag);
       
       
    }   
    
   else if( ((macro_type >= 1) && (macro_type <= 4)) )    // si se trata de un auto 
    {
        if((objetos[macro_type]-> speed) > 0)
            al_draw_scaled_rotated_bitmap( bitmaps[macro_type],     //Dibujo objeto
                                            16 ,  16 , 
                                            coor_x, (objetos[macro_type]->y) *CUADRADITO_SIZE, (CUADRADITO_SIZE / 16 + 0.7), (CUADRADITO_SIZE / 16 + 0.7), 3.14159265359, 0 );
        else
            al_draw_scaled_bitmap( bitmaps[macro_type] ,    //Dibujo objeto
                0, 0, al_get_bitmap_width(bitmaps[macro_type]), al_get_bitmap_height(bitmaps[macro_type]), 
                coor_x, coor_y, CUADRADITO_SIZE, CUADRADITO_SIZE, 
                0);
    }
    
   else if( macro_type == 5 )  //si se trata de un camion
    {
       if((objetos[macro_type]-> speed) > 0)
       {
           al_draw_scaled_bitmap( bitmaps[macro_type] , //Dibujo objeto
                0, 0, al_get_bitmap_width(bitmaps[macro_type]), al_get_bitmap_height(bitmaps[macro_type]), 
                coor_x, coor_y, 2*CUADRADITO_SIZE, CUADRADITO_SIZE, 
                ALLEGRO_FLIP_HORIZONTAL);
       }
       else
            al_draw_scaled_bitmap( bitmaps[macro_type] ,    //Dibujo objeto
                0, 0, al_get_bitmap_width(bitmaps[macro_type]), al_get_bitmap_height(bitmaps[macro_type]), 
                coor_x, coor_y, 2*CUADRADITO_SIZE, CUADRADITO_SIZE, 
                0); 
    }
    
    else    // si se trata de un tronco o tortugas
    {
       if( (macro_type == 6) || (macro_type == 7) )
       {
           if((objetos[macro_type]-> speed) > 0)
            {
                al_draw_scaled_bitmap( bitmaps[macro_type] ,    //Dibujo objeto
                    0, 0, al_get_bitmap_width(bitmaps[macro_type]), al_get_bitmap_height(bitmaps[macro_type]), 
                    coor_x, coor_y, 3*CUADRADITO_SIZE, CUADRADITO_SIZE, 
                    ALLEGRO_FLIP_HORIZONTAL);
            }
            else
                al_draw_scaled_bitmap( bitmaps[macro_type] ,    //Dibujo objeto
                    0, 0, al_get_bitmap_width(bitmaps[macro_type]), al_get_bitmap_height(bitmaps[macro_type]), 
                    coor_x, coor_y, 3*CUADRADITO_SIZE, CUADRADITO_SIZE, 
                    0);
       }
       
       else
       {
        al_draw_scaled_bitmap( bitmaps[macro_type] ,    //Dibujo objeto
                0, 0, al_get_bitmap_width(bitmaps[macro_type]) , al_get_bitmap_height(bitmaps[macro_type]), 
                coor_x, coor_y, 3*CUADRADITO_SIZE, CUADRADITO_SIZE, 
                0); 
       }
    }
    return coor_x;
}


int load_image(int nivel, int macro_type)   // Carga imagenes en los bitmaps
{
    int random;
    char image[120];

    {
        
    if(macro_type == 0)
    {
        random =(rand()%6);
        sprintf(image, "images/frogs/frog%d.png", random ); //elige imagen para el objeto en base a su nivel, 
    }
    else if(macro_type >= 1 && macro_type <= 4)
    {
        random =(rand()%21);
        sprintf(image, "images/cars/car%d.png", random ); 
    }                                                
    else if (macro_type == 5)
    {
        random =(rand()%10);
        sprintf(image, "images/buses/bus%d.png", random ); 
    }
    else if( macro_type == 6 || macro_type == 7)
        sprintf(image, "images/turtles/turtle.png" );

    else if ( macro_type == 8 || macro_type == 9 )
        sprintf(image, "images/troncos/size3/tronco3.png" );
        
    else if (macro_type == 10)
        sprintf(image, "images/niveles/nivel %d/10.png", nivel );
    
    else if(macro_type == 11)
        sprintf(image, "images/menus/1play.png");
    
    else if(macro_type == 12)
        sprintf(image, "images/menus/2nickname.png");
    
    else if(macro_type == 13)
        sprintf(image, "images/menus/3hall.png");
    
    else if(macro_type == 14)
        sprintf(image, "images/menus/4frog.png");
    
    else if(macro_type == 15)
        sprintf(image, "images/menus/5pause.png");
    
    }   // [nacho] carga imagenes al arreglo(repetitivo)
    
    bitmaps[macro_type]= al_load_bitmap(image);     //agrega bitmap(imagen) al arreglo.  
    
    if(!bitmaps[macro_type])
    {
        if(macro_type == FROG)
        {
            printf("No pude cargar la imagen de la objetos[FROG] en el nivel %d.\n", nivel);
            return -1;
        }
        else if(macro_type == BACKGROUND)
        {
            printf("No pude cargar la imagen del background en el nivel %d.\n", nivel);
            return -1;
        }
        printf("No pude cargar la imagen del objeto[%d] en el nivel %d\n", macro_type, nivel);
        return -1;
    }
    return 0;
    
}

static void destruir_bitmaps(void)         //Destruye todos los bitmaps
{
    int i;
    for(i = 0; i <= OBJ_MAX + MENUS_MAX; i++)
    {
        al_destroy_bitmap(bitmaps[i]);      //función de allegro
    }
    printf("Se destruyeron los bitmpas\n");
}

static int go_to_main_menu(void)
{
    int nivel;                          // dato que permite saber en que nivel estamos. por defecto es el 0
    static int init_toggle = 0;         // dato que permite inicializar los objetos una sola vez
    
    /*  ******************************  Pantalla de menus   *********************************************************************************  */
    
    if(menu_status > 0)    //Si es distinto de 0 estoy en algun menu.
    {
        int x; int y;
    
        /*  Usaria switches aca en ves de if(menu_status == 1)  */
        if (menu_status == 1)   // Menu principal
        {
            
            /*      Unicamente se inicia para los menus una vez    */
            
            if (init_toggle == 0)
            {
                nivel = 0; 
                load_sound_effects(0);      //cargo effectos de sonido + musica
                initialize_objects(nivel);
                init_toggle = 1;
            }

            /*      Unicamente se inicia para los menus una vez     */
            
            
            al_set_target_bitmap(al_get_backbuffer(display));   //selecciono el display (cosa de allegro)
            al_clear_to_color(al_map_rgb(255, 255, 255));       //clear del display, ignorar.
            
            al_draw_scaled_bitmap(bitmaps[MAIN_MENU_1],         //dibujo el menu
                0, 0, al_get_bitmap_width(bitmaps[MAIN_MENU_1]), al_get_bitmap_height(bitmaps[MAIN_MENU_1]), 
                0, 0, SIZE_DISPLAY, SIZE_DISPLAY, 
                0);
            
            if(select_option.pressed != 0)  // si se presiono la BARRA o ENTER
            {
                select_option.pressed = 0;  //reset
                play_sound(3);
                
                if(select_option.y_start == (7*CUADRADITO_SIZE))    //Si se encuentra la seleccion en "PLAY"
                {
                    menu_status = 2;
                    
                    select_option.x_start = 3*CUADRADITO_SIZE; 
                    select_option.y_start = 9*CUADRADITO_SIZE;
                    select_option.x_end = 9*CUADRADITO_SIZE;
                    select_option.y_end = 10*CUADRADITO_SIZE;
                    
                }
                else if(select_option.y_start == (11*CUADRADITO_SIZE))    //Si se encuentra la seleccion en "LEADERBOARDS"
                {
                    menu_status = 3;
                    
                    select_option.x_start = 3*CUADRADITO_SIZE; 
                    select_option.y_start = 14*CUADRADITO_SIZE;
                    select_option.x_end = 7*CUADRADITO_SIZE;
                    select_option.y_end = 15*CUADRADITO_SIZE;
                    
                }
                else
                {
                    return 1;   //Significa que le dio a "EXIT"
                }
            }
            else
            {
                al_draw_rectangle(  select_option.x_start, select_option.y_start, 
                            select_option.x_end , select_option.y_end, al_color_name("yellow"), 4.0);  //selection "actual". Al inicio es PLAY
                al_flip_display();
            }
            
            return 0;
    
        }
        
        else if(menu_status == 2)   // Menu NICKNAME (Vasco)
        {
            al_set_target_bitmap(al_get_backbuffer(display));
            al_clear_to_color(al_map_rgb(0, 255, 0));                       //clear del display, ignorar.
            
            al_draw_scaled_bitmap(bitmaps[MAIN_MENU_2],
            0, 0, al_get_bitmap_width(bitmaps[MAIN_MENU_2]), al_get_bitmap_height(bitmaps[MAIN_MENU_2]), 
            0, 0, SIZE_DISPLAY, SIZE_DISPLAY, 
            0); 
            
            if(select_option.pressed != 0)  // si se presiono la BARRA o ENTER
            {
                select_option.pressed = 0;  //reset para que no se active el prox menu
                play_sound(3);
                
                if(select_option.y_start == (9*CUADRADITO_SIZE))    //Si se encuentra la seleccion en "CHOOSE LEVEL"
                {
                    select_option.x_start = 3*CUADRADITO_SIZE;  //  Se setea la seleccion al "FROG 1"
                    select_option.y_start = 8*CUADRADITO_SIZE;  //
                    select_option.x_end = 6*CUADRADITO_SIZE;    //
                    select_option.y_end = 9*CUADRADITO_SIZE;    //
                    
                    menu_status = 4;
                }
                else    //se selecciono "RETURN"
                {
                    select_option.x_start = 4*CUADRADITO_SIZE;  //  Se setea la seleccion al anterior menu
                    select_option.y_start = 7*CUADRADITO_SIZE;  //
                    select_option.x_end = 8*CUADRADITO_SIZE;    //
                    select_option.y_end = 9*CUADRADITO_SIZE;    //
                    
                    menu_status = 1;   //si no le dio a play, significa que le dio a "RETURN"
                    
                }
            }
            
            else
            {
                al_draw_rectangle(  select_option.x_start, select_option.y_start, 
                            select_option.x_end , select_option.y_end, al_color_name("yellow"), 4.0);  //selection "actual". Al inicio es PLAY
                al_flip_display();
            }
            
            return 0;
            
        }

        else if(menu_status == 3)   //Menu HALL OF FAME (Vasco)
        {
            al_set_target_bitmap(al_get_backbuffer(display));
            al_clear_to_color(al_map_rgb(255, 255, 255));
            
            al_draw_scaled_bitmap(bitmaps[MAIN_MENU_3],
            0, 0, al_get_bitmap_width(bitmaps[MAIN_MENU_3]), al_get_bitmap_height(bitmaps[MAIN_MENU_3]), 
            0, 0, SIZE_DISPLAY, SIZE_DISPLAY, 
            0); 
            
            
            if(select_option.pressed != 0)  // si se presiono la BARRA o ENTER
            {
                select_option.pressed = 0;  //reset para que no se active el prox menu
                play_sound(3);
                
                menu_status = 1;    // de "RETURN" al MENU PRINCIPAL
            
                select_option.x_start = 4*CUADRADITO_SIZE; 
                select_option.y_start = 7*CUADRADITO_SIZE;  
                select_option.x_end = 8*CUADRADITO_SIZE;    
                select_option.y_end = 9*CUADRADITO_SIZE; 
                     
            }
            
            
            else 
            {
                al_draw_rectangle(  select_option.x_start , select_option.y_start, 
                                    select_option.x_end, select_option.y_end, al_color_name("yellow"), 4.0);  //
                al_flip_display();
            }
            
            return 0;
        }

        else if(menu_status == 4)   //Menu CHOOSE FROG 
        {
            al_set_target_bitmap(al_get_backbuffer(display));
            al_clear_to_color(al_map_rgb(0, 0, 255));                       //clear del display, ignorar.
            
            al_draw_scaled_bitmap(bitmaps[MAIN_MENU_4],
            0, 0, al_get_bitmap_width(bitmaps[MAIN_MENU_4]), al_get_bitmap_height(bitmaps[MAIN_MENU_4]), 
            0, 0, SIZE_DISPLAY, SIZE_DISPLAY, 
            0); 
            
            
            if(select_option.pressed != 0)  // si se presiono la BARRA o ENTER
            {
                select_option.pressed = 0;  //reset para que no se active el prox menu
                play_sound(3);
                
                if(select_option.y_start == (14*CUADRADITO_SIZE))   //Si se encuentra la seleccion en "RETURN"
                {
                    menu_status = 2;
                    
                    select_option.x_start = 3*CUADRADITO_SIZE;      // nickname 
                    select_option.y_start = 9*CUADRADITO_SIZE;
                    select_option.x_end = 9*CUADRADITO_SIZE;
                    select_option.y_end = 10*CUADRADITO_SIZE;
                    
                    return 0;   //Vuelvo a display_thread
                }
                
                else if(select_option.y_start == (8*CUADRADITO_SIZE))    //Si se encuentra la seleccion en "FROG 1" o "FROG 4"
                {
                    if(select_option.x_start == (3*CUADRADITO_SIZE))    // FROG 1
                    {
                        printf("Se eligio el FROG 1\n");
                    }
                    else if(select_option.x_start == (7*CUADRADITO_SIZE - CUADRADITO_SIZE/2))   // FROG 4
                    {
                        printf("Se eligio el FROG 4\n");
                    }
                }
                
                else if(select_option.y_start == (10*CUADRADITO_SIZE))    //Si se encuentra la seleccion en " FROG 2" o "FROG 5"
                {
                    if(select_option.x_start == (3*CUADRADITO_SIZE))    // lvl 2
                    {
                        printf("Se eligio el FROG 2\n");
                    }
                    else if(select_option.x_start == (7*CUADRADITO_SIZE - CUADRADITO_SIZE/2))   // lvl 5
                    {
                        printf("Se eligio el FROG 5\n");
                    }
                }
                
                else if(select_option.y_start == (12*CUADRADITO_SIZE))    //Si se encuentra la seleccion en "FROG 3" o "FROG 6"
                {
                    if(select_option.x_start == (3*CUADRADITO_SIZE))    // FROG 3
                    {
                        printf("Se eligio el FROG 3\n");
                    }
                    else if(select_option.x_start == (7*CUADRADITO_SIZE - CUADRADITO_SIZE/2))   // FROG 6
                    {
                        printf("Se eligio el FROG 6\n");
                    }
                }
                
            menu_status = 0;    //arranca el juego   
            
            }
            
            else 
            {
                al_draw_rectangle(  select_option.x_start , select_option.y_start, 
                                    select_option.x_end, select_option.y_end, al_color_name("yellow"), 4.0);  //
                al_flip_display();
            }
            
            return 0;
        }
        
                                    // PAUSE MENU
        
        
        else
        {
            printf("Error en la seleccion de menu\n");
            return 0;
        }
 
    }
  /*  ***********************************************************************************************************************************  */
    
    else if (menu_status == 0)  //Me encuentro en la pantalla de juego
    {
        
        if( draw_level(nivel) == 1 )    //
            return 1;       // significa que done = 1 (fin)
   
        return 0;       // significa que done = 0 (continua)
    }
    
    return 0;   // por si acaso
}

static int draw_level(int nivel)
{
    if(nivel == 0)
    {
        al_set_target_bitmap(al_get_backbuffer(display));   //Selecciono el display
            al_clear_to_color(al_map_rgb(255, 255, 255));         //clear del display, ignorar.
       
        al_draw_scaled_bitmap( bitmaps[BACKGROUND],
            0, 0, al_get_bitmap_width( bitmaps[BACKGROUND] ), al_get_bitmap_height( bitmaps[BACKGROUND] ), //imagen
            0, 0, al_get_display_width(display), al_get_display_height(display), //a que tamaño queres que se dibuje la imagen
            0); //Sin flags podrian usar ALLEGRO_FLIP_HORIZONTAL o ALLEGRO_FLIP_VERTICAL muy utiles
        
        
        int j, water_flag = 0;
        for(j = 1; j < OBJ_MAX; j++)
        {
            move_objects();
                
            int k;
            for(k = 0; k < ( objetos[j] -> screen_rep ); k++)       //dibuja el objeto screen_rep veces
            {
                float x = dibujar_objeto(j, k);        //j es el tipo de objeto, k es el numero de variacion de tal objeto
            }
        }
        
        dibujar_objeto(FROG,0);        //dibujar a la objetos[FROG]. Esto debe suceder ultimo asi su iamgen se superpone a las otras.
            
        al_flip_display();
    }
    else
    {
        printf("Otro lvl\n");
    }
    
    
    return 0;
}

